**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

**1. Estructura d’un programa informàtic.**

**1.1 Projectes de desenvolupament d’aplicacions. Entorn integrats de desenvolupament.**

**Exercici 1:** 

**Cerca al mercat actual un IDE propietari i un de codi lliure on es pugui programar en   ANSI C.** 

Propietari: Microsoft Visual Studio. 

Pot detectat errors i fer sugeriments, també pots fer proves i  connectar-te a una base de dades.

Codi lliure: Eclipse. 

Te un depurador de codi que és molt senzill de fer anar i fàcil, també es poden instal·lar plugins.

**Comenta, a grans trets, les característiques de cada un d’ells.** 

**Creus que el CodeBlocks és un bon IDE per programar en C? Raona la resposta.** 

Si, ja que crec que és més o menys intuïtiu i t’ajuda mostrant-te els errors i els warnings que tens en el codi.

___

**1.2 Blocs d’un programa informàtic.** 

**Exercici 2:** 

**Mostra els blocs de codi d’un programa en ANSI C. Comenta’ls breument. Quins blocs apareixeran com a mínim en tot programa C?**

Els blocs que apareixen son:

Declaració de llibreries 

Declaració de constants 

Declaració de tipus Accions i funcions.

___

**1.3. Comentaris al codi.**

**Exercici 3:** 

**Explica els tipus de comentaris existents en C i fes un algorisme que exemplifiqui el seu ús.**

Existeixen 2 tipus de comentaris en llenguatge C, els d’una línia i els de més d’una. Algorisme exemple d’una línia: 

//Això és un comentari d’una sola línia.

Algorisme exemple de més d’una línia: 

/\*Aquest comentari consta de més d’una línia.\*/

___

**1.4 Variables. Tipus i utilitat.** 

**Exercici 4:** 

**Quins són els tipus de dades simples en C? 
Feu un programa que escrigui a pantalla un valor per cada un dels tipus simples. En els tipus numèrics no heu d’incloure els modificadors short, long, signed i unsigned. Explica i posa un exemple de desbordament d’una variable de tipus enter? Afegiu una captura de la sortida a pantalla.** 

Les dades simples en C són: Els nombres enters (int), els caràcters(char)amb els quals és poden fer cadenes  i els nombres reals (float, double.)

Un desbordament és produeix quan un enter es més gran del que la arquitectura del processador li permet hi mostra el valor negatiu ja que el bit significatiu queda a 1.

![](Exercici_1.4.png) ![](Exercici_1.4.2.png)

___
                                                                                                                   
**1.5 Formats de sortida. Instrucció printf.** 

**Exercici 5:** 

**Feu un programa amb la següent sortida a pantalla:** 

**- El número 77 en octal** 

**- El número 65535 en hexadecimal i en majúscules.**

**- El número 32727 en hexadecimal i en minúscules.** 

**- El número 7325 amb el signe.** 

**- El número 6754 amb deu dígits i farciment de 0s per l’esquerra.** 

**- El número 456.54378 amb 3 decimals.** 

**- El teu nom i cognoms.** 

**- Els primers 8 caràcters de la sortida del punt anterior.** 

Afegiu una captura de la sortida a pantalla.

![](Exercici_1.5.png) ![](Exercici_1.5.2.png)

___

**1.6 Constants. Tipus i utilització.**

**Exercici 6:** 

**Explica que són les constants i per a que serveixen. Feu un programa que exemplifiqui l’ús de constants.** 

Les constants son un valor que no pot ser modificat durant l'execució d'un programa per tant no varien. Com per exemple el valor de e=2.71828182846.

![](Exercici_1.6.png)

___

**1.7. Utilització de variables.**

**Exercici 7:** 

**Explica que són les variables i per a que serveixen. Relaciona la memòria de la computadora per explicar-ho. Posa’n exemples. Quan utilitzem les variables? Quina relació hi ha entre una variable i la seva adreça de memòria?** 

Les variables són un valor  que s’emmagatzema en la memòria que té un identificador o nom simbòlic per a aquest.

Un exemple de variable seria un numero, el nom d’una cosa,... Podem utilitzar variables per a ficar algun procés que és repeteix.

La relació que hi ha entre una variable i l’ adreça de memòria és el tipus de dades de la  variable ja que és així com s’assigna la quantitat de memòria.

___

**1.8 Operadors del llenguatge de programació.** 

**Exercici 8:** 

**Cita en forma de taula els operadors relacionals, els aritmètics i els lògics que s’utilitzen en ANSI C. Què és la preferència dels operadors? Posa’n exemples.** 



|Operadors relacionals|Operadors aritmètics|Operadors lògics|
| - | - | - |
|= |\*/ div mod|i|
|<|+-|o|
|≤ ||no|
|>|||
|≥|||
|≠ |||

La preferència dels operadors és l’ordre en el qual s’operen, tenint uns prioritat sobre els altres.

Ex: 4=5≥6 ≠ 7 4\*5+6/3

